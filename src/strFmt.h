/******************************************************************************
 * strFmt.h
 * Copyright (c) 2016 Tom Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Lightweight string formatting functions used to convert numeric types to strings. 
 *
 * Provides minimal but fast numeric-to-string formatting capability. 
 * Designed for small embedded systems that may not have printf(), sprintf() 
 * or fprintf() functions.  Uses addition, subtraction, multiplication and 
 * shift operations.  No division operations are performed.
 */

#ifndef _STR_FMT_H
#define _STR_FMT_H

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdbool.h>
#include <stdint.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/


/******************************************************************************
* Public definitions.
******************************************************************************/

// Set these typedefs to the largest integer data types supported by your system.
typedef int64_t  largest_int_t;   //!< Largest signed integer type
typedef uint64_t largest_uint_t;  //!< Largest unsigned integer type


/******************************************************************************
 * Public functions.
 ******************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief
 * Convert a boolean to a single digit string (0/1).
 *
 * @param dest The string destination address.
 *
 * @param data The boolean data value to convert.
 *
 * @return The length of the converted string (always returns 1).
 */
int strFmtBool(char* dest, bool data);


/**
 * @brief
 * Convert an unsigned integer to a hex digit string.
 *
 * @param dest The string destination address.
 *
 * @param data The data value to convert.
 *
 * @param len The number of data bytes to convert (typically 1 to sizeof(largest_uint_t)).
 *
 * @return The length of the converted string.
 */
int strFmtHex(char* dest, largest_uint_t data, int len);


/**
 * @brief
 * Convert an unsigned integer to a decimal digit string.
 *
 * @param dest The string destination address.
 *
 * @param data The data value to convert.
 *
 * @return The length of the converted string.
 */
int strFmtUint(char* dest, largest_uint_t data);


/**
 * @brief
 * Convert a signed integer to a decimal digit string.
 *
 * @param dest The string destination address.
 *
 * @param data The data value to convert.
 *
 * @return The length of the converted string.
 */
int strFmtInt(char* dest, largest_int_t data);


/**
 * @brief
 * Convert a float to a decimal digit string.
 *
 * The magnitude of the float must be less than 2^32 or a null string
 * is returned in the destination address.
 *
 * Accuracy of the fractional part is not guaranteed. Use at your own risk.
 *
 * @param dest The string destination address.
 *
 * @param data The data value to convert.
 *
 * @param prec The number of digits after the decimal point to convert.
 * No rounding is performed.
 *
 * @return The length of the converted string.
 */
int strFmtFloat(char* dest, float data, int prec);


#ifdef __cplusplus
}
#endif

#endif // _STR_FMT_H
