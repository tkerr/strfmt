/******************************************************************************
 * strFmt.c
 * Copyright (c) 2016 Tom Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Lightweight string formatting functions used to convert numeric types to strings. 
 *
 * Provides minimal but fast numeric-to-string formatting capability. 
 * Designed for small embedded systems that may not have printf(), sprintf() 
 * or fprintf() functions.  Uses addition, subtraction, multiplication and 
 * shift operations.  No division operations are performed.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "strFmt.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/
 
//! @brief Fast integer divide by 10 function.
//!
//! See https://stackoverflow.com/questions/5558492/divide-by-10-using-bit-shifts
//! Original link to Hacker's Delight solution is no longer available.
static largest_uint_t divu10(largest_uint_t n);

//! @brief Reverse a string in place.
static void reverse(char* str, int len);


/******************************************************************************
 * Local definitions.
 ******************************************************************************/

//! Floating point conversion magnitude is limited to 2^32.
#define STR_FLOAT_MAX (4294967296.0)


/******************************************************************************
 * Local data.
 ******************************************************************************/

 
/******************************************************************************
 * Public functions.
 ******************************************************************************/

 
/**************************************
 * strFmtBool
 **************************************/
int strFmtBool(char* dest, bool data)
{
    *dest = (data) ? '1' : '0';
    dest[1] = '\0';
    return 1;
}


/**************************************
 * strFmtHex
 **************************************/
int strFmtHex(char* dest, largest_uint_t data, int len)
{
    char asc;
    char bin;
    int  strLen = (len << 1);  // string length = 2 * data length
    int  idx = strLen;
    dest[idx--] = '\0';        // string terminator
    for (; idx >= 0; --idx)
    {
        bin = (char)(data & 0x0F);
        asc = (bin < 10) ? (bin + '0') : (bin - 10 + 'A');
        dest[idx] = asc;
        data >>= 4;
    }
    return strLen;
}


/**************************************
 * strFmtUint
 **************************************/
int strFmtUint(char* dest, largest_uint_t data)
{
    int strLen;
    largest_uint_t q;  // quotient
    largest_uint_t r;  // remainder
    char* p = dest;
    do
    {
        q = divu10(data);
        r = (data - (q * 10UL));
        *p++ = (char)r + '0';
        data = q;
    } while (data > 0);
    strLen = (int)(p - dest);
    reverse(dest, strLen);
    return strLen;
}


/**************************************
 * strFmtInt
 **************************************/
int strFmtInt(char* dest, largest_int_t data)
{
    int strLen = 0;
    if (data < 0)
    {
        data = -data;
        dest[strLen++] = '-';
    }
    strLen += strFmtUint(&dest[strLen], (largest_uint_t)data);
    return strLen;
}


/**************************************
 * strFmtFloat
 **************************************/
int strFmtFloat(char* dest, float data, int prec)
{
    bool  sign = (data < 0);
    int   strLen = 0;
    float frac;
    
    if (prec > 10) prec = 10;  // Limit precision to a reasonable value
    
    if (sign)
    {
        data = -data;  // Convert to positive number
    }
    
    if (data < STR_FLOAT_MAX)
    {
        if (sign)
        {
            dest[strLen++] = '-';  // Insert negative sign
        }
        
        // Convert the whole number portion.
        strLen += strFmtUint(&dest[strLen], (largest_uint_t)data);
        
        // Convert the fractional portion one digit at a time.
        if (prec > 0)
        {
            dest[strLen++] = '.';
            frac = data - (largest_uint_t)data;
            for (; prec > 0; --prec)
            {
                frac *= 10.0;
                strLen += strFmtUint(&dest[strLen], (largest_uint_t)frac);
                frac -= (largest_uint_t)frac;
            }
        }
    }
    dest[strLen] = '\0';  // Add string terminator
    return strLen;
}


/******************************************************************************
 * Private functions.
 ******************************************************************************/
 
/**************************************
 * divu10
 **************************************/
static largest_uint_t divu10(largest_uint_t n) 
{
    largest_uint_t q;
    largest_uint_t r;
    q = (n >> 1) + (n >> 2);
    q = q + (q >> 4);
    q = q + (q >> 8);
    q = q + (q >> 16);
    q = q >> 3;
    r = n - q * 10;
    return q + ((r + 6) >> 4);
}


/**************************************
 * reverse
 **************************************/
static void reverse(char* str, int len)
{
    int beg = 0;
    int end = len - 1;
    char* temp = &str[len];  // Temporary holding area
    
    while (beg < end)
    {
        *temp = str[beg];
        str[beg] = str[end];
        str[end] = *temp;
        beg++;
        end--;
    }

    *temp = '\0';  // Replace string terminator
}


// End of file.
